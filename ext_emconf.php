<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'League of Legends Clash Manager',
    'description' => 'League of Legends Clash Manager for TYPO3 11',
    'category' => 'plugin',
    'author' => 'Moritz Kiehl',
    'author_email' => 'kiehlmoritz@gmail.com',
    'author_company' => '',
    'version' => '0.0.1',
    'state' => 'alpha'
];
