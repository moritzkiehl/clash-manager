<?php


namespace MoritzKiehl\ClashManager\Service;


use RiotAPI\LeagueAPI\Objects\MatchDto;
use TYPO3\CMS\Core\SingletonInterface;

class MatchService implements SingletonInterface
{
    /**
     * @param MatchDto $match The match witch should be checked for additional players
     * @param array $players An array of player names which should appear in the match
     * @return bool
     */
    public function filterMatchForAdditionalPlayers(MatchDto $match, array $players)
    {
        foreach ($match->participantIdentities as $participant) {
            $summonerName = $participant->player->summonerName;
            foreach ($players as $key => $player) {
                if ($summonerName == $player){
                    unset($players[$key]);
                }
            }
            if (empty($players)){
                return true;
            }
        }
        return false;
    }

}