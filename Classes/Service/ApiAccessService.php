<?php

namespace MoritzKiehl\ClashManager\Service;

use RiotAPI\Base\Definitions\Region;
use RiotAPI\LeagueAPI\LeagueAPI;
use RiotAPI\LeagueAPI\Objects\MatchDto;
use RiotAPI\LeagueAPI\Objects\MatchReferenceDto;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use MoritzKiehl\ClashManager\Service\MatchService;

class ApiAccessService implements \TYPO3\CMS\Core\SingletonInterface
{
    private const routes = [
        "EUROPE" => "https://europe.api.riotgames.com",
        "AMERICAS" => "https://americas.api.riotgames.com",
        "ASIA" => "https://asia.api.riotgames.com",
        "EUW1" => "https://euw1.api.riotgames.com",
        "EUN1" => "https://eun1.api.riotgames.com",
        "BR1" => "https://br1.api.riotgames.com",
        "JP1" => "https://jp1.api.riotgames.com",
        "KR" => "https://kr.api.riotgames.com",
        "LA1" => "https://la1.api.riotgames.com",
        "LA2" => "https://la2.api.riotgames.com",
        "NA1" => "https://na1.api.riotgames.com",
        "OC1" => "https://oc1.api.riotgames.com",
        "TR1" => "https://tr1.api.riotgames.com",
        "RU" => "https://ru.api.riotgames.com"
    ];

    private const endpoints = [
        'SUMMONER' => 'lol/summoner/v4/summoners/by-name',
        'MATCHLIST' => '/lol/match/v4/matchlists/by-account'
    ];

    //Paths to Static Data
    private const QUEUES = "https://static.developer.riotgames.com/docs/lol/queues.json";
    private const MAPS = "https://static.developer.riotgames.com/docs/lol/maps.json";
    private const GAMEMODES = "https://static.developer.riotgames.com/docs/lol/gameModes.json";
    private const GAMETYPES = "https://static.developer.riotgames.com/docs/lol/gameTypes.json";

    private $api;

    /** @var MatchService $matchService */
    private $matchService;

    /**
     * @param MatchService $matchService
     */
    public function injectMatchService(MatchService $matchService)
    {
        $this->matchService = $matchService;
    }

    public function __construct()
    {
        //get api key from extension configuration
        $apiKey = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('clash_manager', 'apiKey');
        $this->api = new LeagueAPI([
            //  Your API key, you can get one at https://developer.riotgames.com/
            LeagueAPI::SET_KEY => $apiKey,
            //  Target region (you can change it during lifetime of the library instance)
            LeagueAPI::SET_REGION => Region::EUROPE_WEST,
        ]);
    }

    /**
     * Base function to communicate with the Riot API
     * Endpoints and parameters can be found at {@link https://developer.riotgames.com/apis}
     *
     * @param string $region Target for the API Call mapped in class constants
     * @param string $endpoint Endpoint of the riot api to be called
     * @param string $parameter Parameters to be added to the request
     */
    public function executeRequest(string $region, string $endpoint, string $parameter = '')
    {

    }

    /**
     * @param array $players An array with summoner names.
     * It should looke like this $requiredPlayers = ['Coatî','Mr Waschbäär','Mrs Waschbäär','Shinumeister'];
     * @param int $queueType Integer representing queue type {@link http://static.developer.riotgames.com/docs/lol/queues.json}
     * 420 = Solo
     * 440 = 5v5 Flex Summoners Rift
     * 700 = Clash
     */
    public function getGameForTeamForQueueType(array $players, int $queueType)
    {
        $summoner = $this->api->getSummonerByName(array_shift($players));
        $matchlist = $this->api->getMatchlistByAccount($summoner->accountId, $queueType);
        /** @var MatchDto[] $matchsWithAllPlayers */
        $matchsWithAllPlayers = [];
        /** @var MatchReferenceDto $match */
        foreach ($matchlist as $match) {
            $matchData = $this->api->getMatch($match->gameId);
            if ($this->matchService->filterMatchForAdditionalPlayers(
                $matchData,
                $players
            )) {
                $matchsWithAllPlayers[$matchData->gameId] = [
                    'matchData' => $matchData,
                    'matchTimeline' => $this->api->getMatchTimeline($matchData->gameId)
                ];
            }
        }
        return $matchsWithAllPlayers;
    }
}