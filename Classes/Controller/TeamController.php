<?php


namespace MoritzKiehl\ClashManager\Controller;


use MoritzKiehl\ClashManager\Service\ApiAccessService;

class TeamController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var ApiAccessService $apiAccessService
     */
    private $apiAccessService;

    /**
     * @param ApiAccessService $apiAccessService
     */
    public function injectApiAccessService(ApiAccessService $apiAccessService)
    {
        $this->apiAccessService = $apiAccessService;
    }

    public function OverviewAction()
    {
        $players = ['Der Langschläfer', 'Coatî', 'Mr Waschbäär', 'Mrs Waschbäär', 'Shinumeister'];
        $result = $this->apiAccessService->getGameForTeamForQueueType($players,700);
        $this->view->assign('result', $result);
    }
}