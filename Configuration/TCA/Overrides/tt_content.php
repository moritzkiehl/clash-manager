<?php

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'ClashManager',
            'TeamOverview',
            'Overview for your Clash Team'
        );
    }
);