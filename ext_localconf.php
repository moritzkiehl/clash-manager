<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(
    function ()
    {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'ClashManager',
            'TeamOverview',
            [
                \MoritzKiehl\ClashManager\Controller\TeamController::class => 'overview',
            ],
            // non-cacheable actions
            [
                \MoritzKiehl\ClashManager\Controller\TeamController::class => 'overview',
            ]
        );
    }
);